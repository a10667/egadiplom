﻿using MasterTester.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MasterTester
{
    class Account
    {
        public List<Answer> Answers = new List<Answer>();
        public List<Test> AnswerTests = new List<Test>();

        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public bool isAdmin { get; set; }

        public user User { get; set; }
        public MasterTester.Models.Group UserGroup { get; }

        public enum Role
        {
            Admin = 2,
            User = 1
        }
        public Account()
        {
#if DEBUG
            using (var db = new testEntities()) 
            { 
                
                var user = db.user.First();
                var userinfo = user.user_info.First(u => u.user_id == user.id);
                Name = $"{userinfo.firstname} {userinfo.secondname} {userinfo.lastname}";
                Login = user.login;
                Password = user.password;
                User = user;

                foreach (var a in db.user_answer)
                {
                    Answers.Add(new Answer() { 
                        Id = a.answer_id,
                        isTrue = (bool)a.answer.is_right,
                        Text = a.is_free ? a.value : a.answer.value
                    });
                }
            }
#else
            this.Name = "noName";
            this.Login = "noName";
            this.Password = "null";
            this.isAdmin = false;
#endif

        }

        public Account(string name, string login, string password, bool role)
        {
            this.Name = name;
            this.Login = login;
            this.Password = password;
            this.isAdmin = role;
        }

        public Account(string name, string login, string password, bool role, user user, MasterTester.Models.Group usergroup)
        {
            this.Name = name;
            this.Login = login;
            this.Password = password;
            this.isAdmin = role;
            this.User = user;
            this.UserGroup  = usergroup;
            
            foreach (var a in user.user_answer)
            {
                Answers.Add(new Answer()
                {
                    Id = a.answer.id,
                    isTrue = (bool)a.answer.is_right,
                    Text = a.is_free? a.value: a.answer.value
                });
            }
        }

        public override string ToString()
        {
            var uu = UserGroup;

            return $"{Name} {uu.Year} {uu.Name}";
        }


        public static implicit operator Account(user user) 
        {
            var userinfo = user.user_info.FirstOrDefault(ui => ui.user_id == user.id);
            if (userinfo == null)
            {
                throw new NullReferenceException(nameof(userinfo));
            }
            var group = user.user_group.FirstOrDefault();
            var usergroup = new Models.Group();
            if (group == null)
            {
                usergroup.Name = "Admin";
            }
            else
            {

                usergroup.Id = group.group_id;
                usergroup.Name = group.group.abbriviation;
                usergroup.Year = group.year;
               
            }
            return new Account($"{userinfo.firstname} {userinfo.secondname} {userinfo.lastname}", user.login, user.password, user.role.name == "admin", user, usergroup);
        }
    }
}
