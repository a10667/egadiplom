﻿using MasterTester.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterTester
{
    public partial class Creator : Form
    {
        public Creator()
        {
            InitializeComponent();
        }

        public List<string> code;

        private void Creator_Load(object sender, EventArgs e)
        {
            Code.Clear();
            if (true)
            {
                NewTest();
            }
        }

        void NewTest()
        {
            Code.Text = "\r\nTEST = Название теста\r\n{";

        }

        void OldTest()
        {

        }

        private void AddOneTrue_Click(object sender, EventArgs e)
        {
            Code.Text += "\r\n\tQUESTION_ONE_TRUE = Тут писать вопрос" +
                "\r\n\t{" +
                "\r\n\t\tANSWER_TRUE = Тут писать верный ответ" +
                "\r\n\t\tANSWER_FALSE = Тут писать неверный ответ" +
                "\r\n\t\tANSWER_FALSE = Ответов может быть до пяти штук" +
                "\r\n\t\tANSWER_FALSE = И в любом порядке" +
                "\r\n\t\tANSWER_FALSE = Главное чтобы один из них был TRUE" +
                "\r\n\t}\r\n";
        }

        private void AddSomeTrue_Click(object sender, EventArgs e)
        {
            Code.Text += "\r\n\tQUESTION_SOME_TRUE = Тут писать вопрос" +
                   "\r\n\t{" +
                   "\r\n\t\tANSWER_TRUE = Тут писать верный ответ" +
                   "\r\n\t\tANSWER_FALSE = Тут писать неверный ответ" +
                   "\r\n\t\tANSWER_TRUE = Ответов может неограниченное количество" +
                   "\r\n\t\tANSWER_FALSE = И в любом порядке" +
                   "\r\n\t\tANSWER_TRUE = Главное чтобы хотя бы один из них был TRUE" +
                   "\r\n\t}\r\n";
        }

        private void AddFreeAnswer_Click(object sender, EventArgs e)
        {
            Code.Text += "\r\n\tQUESTION_FREE_ANSWER = Тут писать вопрос" +
                   "\r\n\t{" +
                   "\r\n\t\tANSWER_TRUE = Тут писать верный ответ, он может быть только один" +
                   "\r\n\t}\r\n";
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            List<string> code = new List<string>();
            foreach (var s in Code.Lines)
            {
                code.Add(s);
            }
            code.Add("}");
            code.Add("");

            try
            {
                var tests = Temp.ParseTest(code.ToArray());
                using (var db = new testEntities())
                {
                    foreach (var test in tests)
                    {
                        db.test.Add(new Entities.test() { name = test.Text, is_open = true });
                        db.SaveChanges();
                        for (int i = 0; i < test.Questions.Count; i++)
                        {
                            Question q = test.Questions[i];
                            var qq = new question()
                            {
                                name = q.Text,
                                test_id = db.test.OrderByDescending(x => x.id).First().id,
                                question_type_id = (int?)q.Type
                            };
                            db.question.Add(qq);
                            db.SaveChanges();
                            foreach (var a in q.Answers)
                            {
                                var aa = new answer()
                                {
                                    question_id = db.question.OrderByDescending(x => x.id).First().id,
                                    value = a.Text,
                                    is_right = a.isTrue
                                };


                                db.answer.Add(aa);
                                db.SaveChanges();
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SaveButton.Enabled = false;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            Code.Text = "\r\nTEST = Название теста\r\n{";
        }

        private void Code_TextChanged(object sender, EventArgs e)
        {
            SaveButton.Enabled = true;
        }

        private void Creator_FormClosing(object sender, FormClosingEventArgs e)
        {
            ForAdmin.isCountTestChanged = true;
        }
    }
}
