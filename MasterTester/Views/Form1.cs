﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using MasterTester.Entities;
using MasterTester.Models;
using Microsoft.Data.SqlClient;

namespace MasterTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadUsers();
            AvgScoreLabel.Enabled = false;
            AvgScoreLabel.Visible = false;
            using (var db = new testEntities())
            {
                GroupBox.Items.Add(new Group
                {
                    Year = 0,
                    Name = "Все группы"
                });
                GroupBox.SelectedIndex = 0;

                var groupsInMem = new List<Group>();
                foreach (var userGroup in db.user_group)
                {
                    var g = new Group()
                    {
                        Id = userGroup.group_id,
                        Year = userGroup.year,
                        Name = userGroup.group.abbriviation,
                        Accounts = new List<Account>()

                    };
                    var groupInTmp = groupsInMem.FirstOrDefault(gm => gm.Id == g.Id && gm.Year == g.Year);
                    if (groupInTmp is null)
                    {
                        g.Accounts.Add((Account)userGroup.user);
                        groupsInMem.Add(g);
                    }
                    else
                    {
                        groupInTmp.Accounts.Add((Account)userGroup.user);
                    }

                }

                foreach (var g in groupsInMem)
                {
                    GroupBox.Items.Add(g);
                }


               
                foreach (var test in db.test)
                {
                    TestComboBox.Items.Add(TestFactory.CreateFrom(test));
                }
            }
        }

        private void LoadUsers()
        {

            using (var db = new testEntities())
            {
                UsersList.Items.Clear();
                foreach (var u in db.user.Where(u => u.role.name != "admin"))
                {
                    var cachedUser = (Account)u;
                    UsersList.Items.Add(cachedUser);
                    //Temp.accounts.Add(cachedUser);
                }
               
            }

            UsersList.SelectedItem = UsersList.Items[0];
        }

        private void UsersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            AvgScoreLabel.Enabled = false;
            AvgScoreLabel.Visible = AvgScoreLabel.Enabled;
            var user = (Account)UsersList.SelectedItem;
            if (user is null) return;
            chart1.Series.Clear();
            chart1.Legends.Clear();
            chart1.Series.Add(new Series());
            SettupSeries(chart1.Series[0], SeriesChartType.Line);
            chart1.Series[0].IsXValueIndexed = true;
            chart1.Series[0].BorderWidth = 5;

            //chart1.ChartAreas.Clear();
            //chart1.Series[0].XValueType = ChartValueType.DateTime;
            //chart1.ChartAreas.Add(new ChartArea()); // In some cases the winforms designer adds this already
            //chart1.ChartAreas[0].AxisX.LabelStyle.Format = "yyyy-MM-dd";
            using (var db = new testEntities())

            {
                foreach (var score in db.score.Where(s => s.user_id == user.User.id))
                {

                    var dataPoint2 = new DataPoint();
                    dataPoint2.SetValueXY(score.date, score.value);
                    dataPoint2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                    dataPoint2.IsValueShownAsLabel = true;
                    dataPoint2.Label = score.test.name;
                    dataPoint2.LabelBackColor = System.Drawing.Color.Silver;
                    dataPoint2.LabelBorderColor = System.Drawing.Color.Black;
                    dataPoint2.LabelForeColor = System.Drawing.SystemColors.ActiveCaptionText;
                    dataPoint2.MarkerBorderColor = System.Drawing.Color.White;
                    chart1.Series.First().Points.Add(dataPoint2);
                }
            }

           
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void AllUsersButton_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();
            chart1.Legends.Clear();
            AvgScoreLabel.Enabled = false;
            AvgScoreLabel.Visible = AvgScoreLabel.Enabled;
            using (var db = new testEntities())
            {
                decimal sum = 0;
                int countof5 = 0;
                int countof4 = 0;
                int countof3 = 0;
                int countof2 = 0;
                int countof1 = 0;
                for (int i = 0; i < UsersList.Items.Count; i++)
                {
                    Account user = (Account)UsersList.Items[i];
                    var scores = db.score.Where(s => s.user_id == user.User.id);

                    countof5 += scores.Where(s => s.value == 5).Count();
                    countof4 += scores.Where(s => s.value == 4).Count();
                    countof3 += scores.Where(s => s.value == 3).Count();
                    countof2 += scores.Where(s => s.value == 2).Count();
                    countof1 += scores.Where(s => s.value == 1).Count();


                    chart1.Series.Add(new Series());
                    
                    chart1.Legends.Add(new Legend());
                    chart1.Series[i].LegendText = user.ToString();
                    foreach (var score in scores)
                    {

                        SettupSeries(chart1.Series[i], SeriesChartType.Column);
                        chart1.Series[i].Points.AddXY(score.date, score.value);
                        
                    }
                }

                //int avg = () / (15);
            }
        }

        private static void SettupSeries(Series series, SeriesChartType seriesChartType, ChartValueType xvalue = ChartValueType.Date)
        {
            series.IsXValueIndexed = false;
            series.ChartType = seriesChartType;
            series.XValueType = xvalue;
            series.YValueType = ChartValueType.Int64;
        }

        private void GroupBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            var selectedGroup = (Group)GroupBox.SelectedItem;
            if (selectedGroup is null)
            {
                throw new NullReferenceException(nameof(selectedGroup));
            }

            if (selectedGroup.Year < 1) 
            { 
                LoadUsers();
                AvgScoreLabel.Enabled = false;
                AvgScoreLabel.Visible = AvgScoreLabel.Enabled;

            }
            else
            {
                UsersList.Items.Clear();

                using (var db = new testEntities())
                {
                    foreach (var u in 
                        db.user.Where(u => u.role.name != "admin" && u.user_group.Any(ug => ug.group_id == selectedGroup.Id && ug.year == selectedGroup.Year)))
                    {
                        var cachedUser = (Account)u;
                        UsersList.Items.Add(cachedUser);
                        //Temp.accounts.Add(cachedUser);
                    }
                }
            }
        }

        private void TestComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedTest = (Test)TestComboBox.SelectedItem;
            if (selectedTest is null)
            {
                return;
            }
            AvgScoreLabel.Enabled = true && ((Group)GroupBox.SelectedItem).Year > 0;
            AvgScoreLabel.Visible = AvgScoreLabel.Enabled;


            chart1.Series.Clear();
            chart1.Legends.Clear();
            using (var db = new testEntities())
            {

                int countof5 = 0;
                int countof4 = 0;
                int countof3 = 0;
                int countof2 = 0;
                int countof1 = 0;

                chart1.Series.Add(new Series());
                SettupSeries(chart1.Series.First(), SeriesChartType.Column, ChartValueType.String);
                for (int i = 0; i < UsersList.Items.Count; i++)
                {
                    Account user = (Account)UsersList.Items[i];
                    var scores = db.score.Where(s => s.user_id == user.User.id).Where(s => s.test_id == selectedTest.IdInt);

                    countof5 += scores.Where(s => s.value == 5).Count();
                    countof4 += scores.Where(s => s.value == 4).Count();
                    countof3 += scores.Where(s => s.value == 3).Count();
                    countof2 += scores.Where(s => s.value == 2).Count();
                    countof1 += scores.Where(s => s.value == 1).Count();

                    foreach (var score in scores)
                    {
                        chart1.Series.First().Points.AddXY(user.Name, score.value);
                        chart1.Series.First().Points.Last().AxisLabel = user.Name;
                    }
                }

                int v = (5 * countof5 + 4 * countof4 + 3 * countof3 + 2 * countof2 + 1 * countof1);
                int v1 = (countof5 + countof4 + countof3 + countof2 + countof1);
                if (v1 == 0) {
                    AvgScoreLabel.Text = string.Empty;
                    AvgScoreLabel.Visible = AvgScoreLabel.Enabled;
                    return; }
                float avg = v / v1;
                AvgScoreLabel.Text = $"Средний бал: {avg}";
            }
        }
    }
}
