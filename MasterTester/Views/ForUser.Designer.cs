﻿namespace MasterTester
{
    partial class ForUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ForUser));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.LabelAccountName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.NextButton = new System.Windows.Forms.Button();
            this.LabelQuestion = new System.Windows.Forms.Label();
            this.FreeAnswerPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.FreeAnswer = new System.Windows.Forms.TextBox();
            this.AnswerOneTrue = new System.Windows.Forms.Panel();
            this.OT5 = new System.Windows.Forms.RadioButton();
            this.OT4 = new System.Windows.Forms.RadioButton();
            this.OT3 = new System.Windows.Forms.RadioButton();
            this.OT2 = new System.Windows.Forms.RadioButton();
            this.OT1 = new System.Windows.Forms.RadioButton();
            this.AnswerSomeTrue = new System.Windows.Forms.CheckedListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TestResultLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.StartTest = new System.Windows.Forms.Button();
            this.NewTestsList = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.FreeAnswerPanel.SuspendLayout();
            this.AnswerOneTrue.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(37)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pictureBox10);
            this.panel1.Controls.Add(this.pictureBox6);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox12);
            this.panel1.Controls.Add(this.LabelAccountName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(407, 67);
            this.panel1.TabIndex = 3;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(229, 21);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(51, 50);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 29;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(339, 21);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(51, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 25;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(284, 21);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(51, 50);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 24;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(174, 21);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(51, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 23;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(119, 21);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(51, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 22;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(64, 21);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(51, 50);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(9, 21);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(51, 50);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 20;
            this.pictureBox12.TabStop = false;
            // 
            // LabelAccountName
            // 
            this.LabelAccountName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelAccountName.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelAccountName.ForeColor = System.Drawing.Color.White;
            this.LabelAccountName.Location = new System.Drawing.Point(77, 2);
            this.LabelAccountName.Name = "LabelAccountName";
            this.LabelAccountName.Size = new System.Drawing.Size(325, 19);
            this.LabelAccountName.TabIndex = 9;
            this.LabelAccountName.Text = "NAME";
            this.LabelAccountName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 23);
            this.label2.TabIndex = 8;
            this.label2.Text = "Пользователь: ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(37)))));
            this.groupBox3.Controls.Add(this.ProgressBar);
            this.groupBox3.Controls.Add(this.NextButton);
            this.groupBox3.Controls.Add(this.LabelQuestion);
            this.groupBox3.Controls.Add(this.FreeAnswerPanel);
            this.groupBox3.Controls.Add(this.AnswerOneTrue);
            this.groupBox3.Controls.Add(this.AnswerSomeTrue);
            this.groupBox3.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(1, 260);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(406, 289);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Тестирование";
            // 
            // ProgressBar
            // 
            this.ProgressBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.ProgressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.ProgressBar.Location = new System.Drawing.Point(7, 250);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(193, 30);
            this.ProgressBar.TabIndex = 14;
            // 
            // NextButton
            // 
            this.NextButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.NextButton.Enabled = false;
            this.NextButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.NextButton.Font = new System.Drawing.Font("Bahnschrift SemiBold", 11.25F, System.Drawing.FontStyle.Bold);
            this.NextButton.ForeColor = System.Drawing.Color.White;
            this.NextButton.Location = new System.Drawing.Point(205, 249);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(194, 32);
            this.NextButton.TabIndex = 12;
            this.NextButton.Text = "Следующий >>>";
            this.NextButton.UseVisualStyleBackColor = false;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // LabelQuestion
            // 
            this.LabelQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.LabelQuestion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelQuestion.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelQuestion.ForeColor = System.Drawing.Color.White;
            this.LabelQuestion.Location = new System.Drawing.Point(6, 16);
            this.LabelQuestion.Name = "LabelQuestion";
            this.LabelQuestion.Size = new System.Drawing.Size(393, 69);
            this.LabelQuestion.TabIndex = 10;
            this.LabelQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FreeAnswerPanel
            // 
            this.FreeAnswerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.FreeAnswerPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FreeAnswerPanel.Controls.Add(this.label1);
            this.FreeAnswerPanel.Controls.Add(this.FreeAnswer);
            this.FreeAnswerPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.FreeAnswerPanel.Location = new System.Drawing.Point(6, 94);
            this.FreeAnswerPanel.Name = "FreeAnswerPanel";
            this.FreeAnswerPanel.Size = new System.Drawing.Size(393, 151);
            this.FreeAnswerPanel.TabIndex = 13;
            this.FreeAnswerPanel.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введите свой ответ:";
            // 
            // FreeAnswer
            // 
            this.FreeAnswer.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FreeAnswer.Location = new System.Drawing.Point(4, 23);
            this.FreeAnswer.Multiline = true;
            this.FreeAnswer.Name = "FreeAnswer";
            this.FreeAnswer.Size = new System.Drawing.Size(384, 123);
            this.FreeAnswer.TabIndex = 0;
            this.FreeAnswer.TextChanged += new System.EventHandler(this.FreeAnswer_TextChanged);
            // 
            // AnswerOneTrue
            // 
            this.AnswerOneTrue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.AnswerOneTrue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerOneTrue.Controls.Add(this.OT5);
            this.AnswerOneTrue.Controls.Add(this.OT4);
            this.AnswerOneTrue.Controls.Add(this.OT3);
            this.AnswerOneTrue.Controls.Add(this.OT2);
            this.AnswerOneTrue.Controls.Add(this.OT1);
            this.AnswerOneTrue.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AnswerOneTrue.ForeColor = System.Drawing.Color.White;
            this.AnswerOneTrue.Location = new System.Drawing.Point(6, 94);
            this.AnswerOneTrue.Name = "AnswerOneTrue";
            this.AnswerOneTrue.Size = new System.Drawing.Size(393, 151);
            this.AnswerOneTrue.TabIndex = 9;
            this.AnswerOneTrue.Visible = false;
            // 
            // OT5
            // 
            this.OT5.AutoSize = true;
            this.OT5.Location = new System.Drawing.Point(3, 122);
            this.OT5.Name = "OT5";
            this.OT5.Size = new System.Drawing.Size(44, 22);
            this.OT5.TabIndex = 12;
            this.OT5.Text = "OT5";
            this.OT5.UseVisualStyleBackColor = true;
            this.OT5.CheckedChanged += new System.EventHandler(this.OT5_CheckedChanged);
            // 
            // OT4
            // 
            this.OT4.AutoSize = true;
            this.OT4.Location = new System.Drawing.Point(3, 93);
            this.OT4.Name = "OT4";
            this.OT4.Size = new System.Drawing.Size(44, 22);
            this.OT4.TabIndex = 11;
            this.OT4.Text = "OT4";
            this.OT4.UseVisualStyleBackColor = true;
            this.OT4.CheckedChanged += new System.EventHandler(this.OT4_CheckedChanged);
            // 
            // OT3
            // 
            this.OT3.AutoSize = true;
            this.OT3.Location = new System.Drawing.Point(3, 63);
            this.OT3.Name = "OT3";
            this.OT3.Size = new System.Drawing.Size(44, 22);
            this.OT3.TabIndex = 10;
            this.OT3.Text = "OT3";
            this.OT3.UseVisualStyleBackColor = true;
            this.OT3.CheckedChanged += new System.EventHandler(this.OT3_CheckedChanged);
            // 
            // OT2
            // 
            this.OT2.AutoSize = true;
            this.OT2.Location = new System.Drawing.Point(3, 33);
            this.OT2.Name = "OT2";
            this.OT2.Size = new System.Drawing.Size(44, 22);
            this.OT2.TabIndex = 9;
            this.OT2.Text = "OT2";
            this.OT2.UseVisualStyleBackColor = true;
            this.OT2.CheckedChanged += new System.EventHandler(this.OT2_CheckedChanged);
            // 
            // OT1
            // 
            this.OT1.AutoSize = true;
            this.OT1.Location = new System.Drawing.Point(3, 3);
            this.OT1.Name = "OT1";
            this.OT1.Size = new System.Drawing.Size(42, 22);
            this.OT1.TabIndex = 8;
            this.OT1.Text = "OT1";
            this.OT1.UseVisualStyleBackColor = true;
            this.OT1.CheckedChanged += new System.EventHandler(this.OT1_CheckedChanged);
            // 
            // AnswerSomeTrue
            // 
            this.AnswerSomeTrue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.AnswerSomeTrue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerSomeTrue.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AnswerSomeTrue.ForeColor = System.Drawing.Color.White;
            this.AnswerSomeTrue.FormattingEnabled = true;
            this.AnswerSomeTrue.Location = new System.Drawing.Point(6, 89);
            this.AnswerSomeTrue.Name = "AnswerSomeTrue";
            this.AnswerSomeTrue.Size = new System.Drawing.Size(393, 156);
            this.AnswerSomeTrue.TabIndex = 13;
            this.AnswerSomeTrue.Visible = false;
            this.AnswerSomeTrue.SelectedIndexChanged += new System.EventHandler(this.AnswerSomeTrue_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(37)))));
            this.groupBox2.Controls.Add(this.TestResultLabel);
            this.groupBox2.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(207, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 192);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Результаты теста";
            // 
            // TestResultLabel
            // 
            this.TestResultLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.TestResultLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TestResultLabel.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TestResultLabel.ForeColor = System.Drawing.Color.White;
            this.TestResultLabel.Location = new System.Drawing.Point(7, 20);
            this.TestResultLabel.Name = "TestResultLabel";
            this.TestResultLabel.Size = new System.Drawing.Size(186, 165);
            this.TestResultLabel.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(37)))));
            this.groupBox1.Controls.Add(this.StartTest);
            this.groupBox1.Controls.Add(this.NewTestsList);
            this.groupBox1.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(1, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 192);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Все тесты";
            // 
            // StartTest
            // 
            this.StartTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.StartTest.Enabled = false;
            this.StartTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.StartTest.Font = new System.Drawing.Font("Bahnschrift SemiBold", 11.25F, System.Drawing.FontStyle.Bold);
            this.StartTest.ForeColor = System.Drawing.Color.White;
            this.StartTest.Location = new System.Drawing.Point(7, 144);
            this.StartTest.Name = "StartTest";
            this.StartTest.Size = new System.Drawing.Size(186, 41);
            this.StartTest.TabIndex = 14;
            this.StartTest.Text = "Пройти";
            this.StartTest.UseVisualStyleBackColor = false;
            this.StartTest.Click += new System.EventHandler(this.StartTest_Click);
            // 
            // NewTestsList
            // 
            this.NewTestsList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.NewTestsList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NewTestsList.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewTestsList.ForeColor = System.Drawing.Color.White;
            this.NewTestsList.FormattingEnabled = true;
            this.NewTestsList.ItemHeight = 18;
            this.NewTestsList.Location = new System.Drawing.Point(7, 20);
            this.NewTestsList.Name = "NewTestsList";
            this.NewTestsList.Size = new System.Drawing.Size(186, 110);
            this.NewTestsList.TabIndex = 0;
            this.NewTestsList.SelectedIndexChanged += new System.EventHandler(this.NewTestsList_SelectedIndexChanged);
            // 
            // ForUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(17)))), ((int)(((byte)(23)))));
            this.ClientSize = new System.Drawing.Size(409, 565);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ForUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Мастер Тестер";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ForUser_FormClosing);
            this.Load += new System.EventHandler(this.ForUser_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.FreeAnswerPanel.ResumeLayout(false);
            this.FreeAnswerPanel.PerformLayout();
            this.AnswerOneTrue.ResumeLayout(false);
            this.AnswerOneTrue.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LabelAccountName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox NewTestsList;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.CheckedListBox AnswerSomeTrue;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Label LabelQuestion;
        private System.Windows.Forms.RadioButton OT1;
        private System.Windows.Forms.Panel AnswerOneTrue;
        private System.Windows.Forms.RadioButton OT5;
        private System.Windows.Forms.RadioButton OT4;
        private System.Windows.Forms.RadioButton OT3;
        private System.Windows.Forms.RadioButton OT2;
        private System.Windows.Forms.Panel FreeAnswerPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FreeAnswer;
        private System.Windows.Forms.Label TestResultLabel;
        private System.Windows.Forms.Button StartTest;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox12;
    }
}