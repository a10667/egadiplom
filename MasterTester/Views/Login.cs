﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MasterTester.Entities;
using Microsoft.Data.SqlClient;

namespace MasterTester
{
    public partial class FormLogin : Form
    {
        ForAdmin admin = new ForAdmin();
        ForUser user = new ForUser();

        public FormLogin()
        {
            InitializeComponent();
            admin.FormClosed += (object s, FormClosedEventArgs ev) => { this.Show(); };
            user.FormClosed += (object s, FormClosedEventArgs ev) => { this.Show(); };

        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
           
            ButtonLogin.Enabled = false;
            this.Text = Temp.VERSION;

        }

        private void ButtonSignup_Click(object sender, EventArgs e)
        {
            Signup signup = new Signup();
            signup.ShowDialog();
        }

        private void TextLogin_TextChanged(object sender, EventArgs e)
        {
            // Проверить, заполнены ли все текстовые поля
            bool allFieldsFilled = TextLogin.Text.Trim().Length > 0 &&
                                   TextPassword.Text.Trim().Length > 0;

            // Разблокировать кнопку регистрации, если все поля заполнены
            ButtonLogin.Enabled = allFieldsFilled;
        }

        private void TextPassword_TextChanged(object sender, EventArgs e)
        {
            // Проверить, заполнены ли все текстовые поля
            bool allFieldsFilled = TextLogin.Text.Trim().Length > 0 &&
                                   TextPassword.Text.Trim().Length > 0;

            // Разблокировать кнопку регистрации, если все поля заполнены
            ButtonLogin.Enabled = allFieldsFilled;
        }

        private async void ButtonLogin_Click(object sender, EventArgs e)
        {
            bool isLoginSuccessful = false;
            using (var db = new testEntities())
            {
                var foundUser = await db.user.FirstOrDefaultAsync(user => user.password == TextPassword.Text && user.login == TextLogin.Text);
                isLoginSuccessful = foundUser != null;
                if (isLoginSuccessful)
                {
                    Temp.account = foundUser;
                }
            }

            if (isLoginSuccessful)
            {
                if (Temp.account.isAdmin == true)
                {
                    this.Hide();
                    admin.ShowDialog();
                }
                else
                {
                    this.Hide();
                    user.ShowDialog();
                }
                // Вход выполнен успешно
                IndicatorLabel.ForeColor = Color.Black;
                IndicatorLabel.Text = "Введите ваш логин и пароль или зарегистрируйтесь";
                Console.WriteLine($"{DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")}\tПользователь .");

            }
            else
            {
                // Ошибка входа
                IndicatorLabel.ForeColor = Color.DarkRed;
                IndicatorLabel.Text = "Неправильный логин или пароль";
            }
        }
    }
}
