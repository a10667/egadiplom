﻿using MasterTester.Entities;
using MasterTester.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterTester
{
    public partial class ForUser : Form
    {
        private readonly ScoreRepository _scoreRepository;
        private readonly TestRepository _testRepository;    
        public ForUser()
        {
            _scoreRepository = new ScoreRepository();
            _testRepository = new TestRepository();
            InitializeComponent();
        }

        ~ForUser()
        {
            _scoreRepository.ClearCache();
            _testRepository.ClearCache(); 
        }

        private void ForUser_Load(object sender, EventArgs e)
        {
            //Temp.accounts = Temp.ReadAccountsFromFile();

            TestResultLabel.Text = string.Empty;
            LoadUsers();
            LoadTests();
            LabelAccountName.Text = Temp.account.ToString();
            this.Text = Temp.VERSION;

            UserTestsUpdate();


            foreach (var t in Temp.tests)
            {
                Temp.account.AnswerTests.Add(t);
            }

            OT1.Checked = false;
            OT2.Checked = false;
            OT3.Checked = false;
            OT4.Checked = false;
            OT5.Checked = false;

        }

        public void LoadTests()
        {
            NewTestsList.Items.Clear();
            Temp.tests.Clear();
            using (var db = new testEntities())
            {
                foreach (var item in db.test)
                {
                    var tb = new TestBuilder();
                    tb.WithText(item.name).WithId(item.id);
                    foreach (var q in item.question)
                    {
                        tb.AddQuestion(qq => {
                            foreach (var a in q.answer)
                            {
                                qq.AddAnswer(aa => aa.IsTrue(a.is_right.Value).WithText(a.value).WithId(a.id));
                            }
                            return qq.WithText(q.name).WithId(q.id).WithType(q.question_type.name);
                        });
                    }

                    Test t = (Test)tb;
                    Temp.tests.Add(t);
                    NewTestsList.Items.Add(t);
                }

            }
        }

        void UserTestsUpdate()
        {
            NewTestsList.Items.Clear();

            List<Test> allTests = Temp.tests;

            foreach (Test test in allTests)
            {   
                NewTestsList.Items.Add(test);
            }
        }

        private void LoadUsers()
        {
            
        }

        private void NewTestsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            StartTest.Enabled = false;

            _ = UpdateTestsAsync();
        }

        private async Task UpdateTestsAsync()
        {
            if (NewTestsList.SelectedIndex >= 0)
            {
                Test selectedTest = Temp.account.AnswerTests[NewTestsList.SelectedIndex];
                if (selectedTest != null)
                {
                    int totalQuestions = selectedTest.Questions.Count;
                    int correctAnswers = 0;
                    int incorrectAnswers = 0;
                    int totalAnswers = 0;
                    bool testBeenStarted = false;

                    var wasTestSavedInDb = (await _scoreRepository.FindTestScoreOfUserByIdAsync(Temp.account.User.id, int.Parse(selectedTest.Id))) != null;
                    if (wasTestSavedInDb)
                    {
                        await _testRepository.FillTestWithUserAnswersAsync(selectedTest, Temp.account.User.id);
                    }

                    foreach (Question question in selectedTest.Questions)
                    {
                        foreach (Answer answer in question.Answers)
                        {
                            if (answer.isTrue)
                            {
                                totalAnswers++;
                            }
                            if (answer.isTrue && answer.isUserAnswer)
                            {
                                correctAnswers++;
                            }
                            if (answer.isUserAnswer)
                            {
                                testBeenStarted = true;
                            }
                        }
                    }
              
                    double score = CalculateScore(correctAnswers, totalAnswers);
                    string evaluation = GetEvaluation(score);

                    testBeenStarted =  testBeenStarted || wasTestSavedInDb;

                    if (testBeenStarted)
                    {
                        TestResultLabel.Text = $"Тест: {selectedTest.Text}\n" +
                                               $"Всего вопросов: {totalQuestions}\n" +
                                               $"Правильных ответов: {correctAnswers} из {totalAnswers}\n" +
                                               $"Неправильных ответов: {totalAnswers - correctAnswers} из {totalAnswers}\n" +
                                               $"Оценка: {evaluation}";

                        if (!wasTestSavedInDb) 
                        {
                            using (var db = new testEntities())
                            {
                                if (_currentTest == null)
                                    throw new NullReferenceException(nameof(_currentTest));
                                db.score.Add(new score() { test_id = int.Parse(_currentTest.Id), user_id = Temp.account.User.id, value = (decimal)score, date = DateTime.Now });
                                db.SaveChanges();
                            }
                        }

                    }
                    else
                    {
                        _currentTest = (Test)NewTestsList.SelectedItem;
                        TestResultLabel.Text = "Тест ещё не был пройден";
                        using (var db = new testEntities())
                        {
                            var currentTestId = int.Parse(_currentTest.Id);
                            var wasCompletedBefore = db.score.FirstOrDefault(s => s.user_id == Temp.account.User.id && s.test_id == currentTestId) != null;
                            StartTest.Enabled = true && !wasCompletedBefore;
                        }
 
                    }
                }
            }
        }

        private double CalculateScore(int correctAnswers, int totalQuestions)
        {

            double percentage = (double)correctAnswers / totalQuestions;
            double score = percentage * 5;

            return Math.Round(score, 2); 
        }

        private string GetEvaluation(double score)
        {
            if (score >= 4.5)
            {
                return $"5 ({score})";
            }
            else if (score >= 3.5)
            {
                return $"4 ({score})";
            }
            else if (score >= 2.5)
            {
                return $"3 ({score})";
            }
            else if (score >= 1.5)
            {
                return $"2 ({score})";
            }
            else
            {
                return $"1 ({score})";
            }
        }

        Test currentTest;
        private int currentQuestion;
        int currentTestIndex;
        private Test _currentTest;
        private static Random rng = new Random(DateTime.UtcNow.Millisecond);

        private void StartTest_Click(object sender, EventArgs e)
        {
            currentTest = Temp.account.AnswerTests[NewTestsList.SelectedIndex];
            currentTestIndex = NewTestsList.SelectedIndex;
            currentQuestion = 0;
            NewTestsList.Enabled = false;
            StartTest.Enabled = false;
            NextButton.Enabled = true;

            // shuffle question
            var shuffledQuestionsIdxes = new List<int>();

            for (int i = 0; i < currentTest.Questions.Count; i++)
            {
                var rngIdx = rng.Next(0, currentTest.Questions.Count);
                shuffledQuestionsIdxes.Add(rngIdx);
            }

            var shuffledQuestions = new List<Question>();
            foreach (int i in shuffledQuestionsIdxes)
            {
                shuffledQuestions.Add(PickRandomQuestion(currentTest.Questions));
            }
            currentTest.Questions = shuffledQuestions;

            TestUpdater();
        }

        private Question PickRandomQuestion(List<Question> questions)
        {
            var idx = rng.Next(0, questions.Count);
            var question = questions[idx];
            questions.Remove(question);
            return question;
        }

        private void TestUpdater()
        {

            Question question = currentTest.Questions[currentQuestion];
            LabelQuestion.Text = question.Text;

            if (question.Type == QuestionType.FREE_ANSWER)
            {
                FreeAnswerPanel.Visible = true;
                FreeAnswer.Clear();
            }
            else FreeAnswerPanel.Visible = false;

            if (question.Type == QuestionType.ONE_TRUE)
            {
                AnswerOneTrue.Visible = true;
                OT1.Visible = false; OT2.Visible = false; OT3.Visible = false; OT4.Visible = false; OT5.Visible = false;

                for (int a = 0; a < question.Answers.Count; a++)
                {
                    switch (a)
                    {
                        case 0:
                            OT1.Text = question.Answers[a].Text;
                            OT1.Visible = true;
                            break;

                        case 1:
                            OT2.Text = question.Answers[a].Text;
                            OT2.Visible = true;
                            break;

                        case 2:
                            OT3.Text = question.Answers[a].Text;
                            OT3.Visible = true;
                            break;

                        case 3:
                            OT4.Text = question.Answers[a].Text;
                            OT4.Visible = true;
                            break;

                        case 4:
                            OT5.Text = question.Answers[a].Text;
                            OT5.Visible = true;
                            break;

                        default:
                            break;
                    }
                }
            }
            else
            {
                AnswerOneTrue.Visible = false;
                OT1.Visible = false; OT2.Visible = false; OT3.Visible = false; OT4.Visible = false; OT5.Visible = false;
            }

            if (question.Type == QuestionType.SOME_TRUE)
            {
                AnswerSomeTrue.Visible = true;
                AnswerSomeTrue.Items.Clear();

                foreach (var a in question.Answers)
                {
                    AnswerSomeTrue.Items.Add(a.Text);
                }
            }
            else AnswerSomeTrue.Visible = false;

        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            NextButton.Enabled = false;
            if (currentQuestion < currentTest.Questions.Count - 1)
            {
                using (var db = new testEntities())
                {
                    Question cq = currentTest.Questions[currentQuestion];
                    if (cq.Type == QuestionType.FREE_ANSWER)
                    {
                        var ca = cq.Answers[0];
                        ca.isTrue = true;
                        ca.isUserAnswer = true;

                        SaveUserAnswer(db, ca, true);
                    }

                    if (cq.Type == QuestionType.ONE_TRUE)
                    {
                        if (OT1.Checked)
                        {
                            cq.Answers[0].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[0]);
                        }

                        if (OT2.Checked)
                        {
                            cq.Answers[1].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[1]);
                        }

                        if (OT3.Checked)
                        {
                            cq.Answers[2].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[2]);
                        }

                        if (OT4.Checked)
                        {
                            cq.Answers[3].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[3]);
                        }

                        if (OT5.Checked)
                        {
                            cq.Answers[4].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[4]);
                        }
                    }

                    if (cq.Type == QuestionType.SOME_TRUE)
                    {
                        List<int> checkedIndices = new List<int>();
                        for (int i = 0; i < AnswerSomeTrue.CheckedIndices.Count; i++)
                        {
                            int index = AnswerSomeTrue.CheckedIndices[i];
                            checkedIndices.Add(index);
                        }

                        foreach (int index in checkedIndices)
                        {
                            cq.Answers[index].isUserAnswer = true;

                            SaveUserAnswer(db, cq.Answers[index]);
                        }
                    }


                }

                ProgressBar.Value += 100 / currentTest.Questions.Count;

                
                if (currentQuestion < currentTest.Questions.Count - 1)
                { 
                    currentQuestion++;
                }
                TestUpdater();
            }
            else
            {
                using (var db = new testEntities())
                {
                    Question cq = currentTest.Questions[currentQuestion];
                    if (cq.Type == QuestionType.FREE_ANSWER)
                    {
                        var ca = cq.Answers[0];
                        ca.isTrue = true;
                        ca.isUserAnswer = true;

                        SaveUserAnswer(db, ca, true);
                    }

                    if (cq.Type == QuestionType.ONE_TRUE)
                    {
                        if (OT1.Checked)
                        {
                            cq.Answers[0].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[0]);
                        }

                        if (OT2.Checked)
                        {
                            cq.Answers[1].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[1]);
                        }

                        if (OT3.Checked)
                        {
                            cq.Answers[2].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[2]);
                        }

                        if (OT4.Checked)
                        {
                            cq.Answers[3].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[3]);
                        }

                        if (OT5.Checked)
                        {
                            cq.Answers[4].isUserAnswer = true;
                            SaveUserAnswer(db, cq.Answers[4]);
                        }
                    }

                    if (cq.Type == QuestionType.SOME_TRUE)
                    {
                        List<int> checkedIndices = new List<int>();
                        for (int i = 0; i < AnswerSomeTrue.CheckedIndices.Count; i++)
                        {
                            int index = AnswerSomeTrue.CheckedIndices[i];
                            checkedIndices.Add(index);
                        }

                        // Используйте checkedIndices в вашем коде
                        foreach (int index in checkedIndices)
                        {
                            cq.Answers[index].isUserAnswer = true;

                            SaveUserAnswer(db, cq.Answers[index]);
                        }
                    }

                
                }

                ProgressBar.Value += 100 / currentTest.Questions.Count;

                NewTestsList.Enabled = true;
                NextButton.Enabled = false;
                AnswerOneTrue.Visible = false;
                AnswerSomeTrue.Visible = false;
                FreeAnswerPanel.Visible = false;
                LabelQuestion.Text = "";
                ProgressBar.Value = 0;
                UpdateTestsAsync();
            }
        }

        private void SaveUserAnswer(testEntities db, Answer cq, bool isFree = false)
        {
            var answerInDb = db.answer.FirstOrDefault(a => a.id == cq.Id) ?? throw new NullReferenceException();
            var userInDb = db.user.FirstOrDefault(a => a.id == Temp.account.User.id) ?? throw new NullReferenceException();
            db.user_answer.Add(new user_answer() {
                answer_id = answerInDb.id,
                user_id = userInDb.id,
                is_free = isFree,
                value = isFree? FreeAnswer.Text: null
            });
            cq.Text = isFree ? FreeAnswer.Text :cq.Text; 
            Temp.account.Answers.Add(cq);
            db.SaveChanges();
        }

        private void AnswerSomeTrue_SelectedIndexChanged(object sender, EventArgs e)
        {
            NextButton.Enabled = true;
        }

        private void FreeAnswer_TextChanged(object sender, EventArgs e)
        {
            NextButton.Enabled = true;
        }

        private void OT1_CheckedChanged(object sender, EventArgs e)
        {
            NextButton.Enabled = true;
        }

        private void OT2_CheckedChanged(object sender, EventArgs e)
        {
            NextButton.Enabled = true;
        }

        private void OT3_CheckedChanged(object sender, EventArgs e)
        {
            NextButton.Enabled = true;
        }

        private void OT4_CheckedChanged(object sender, EventArgs e)
        {
            NextButton.Enabled = true;
        }

        private void OT5_CheckedChanged(object sender, EventArgs e)
        {
            NextButton.Enabled = true;
        }

        private void ForUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            var userInCache = Temp.accounts.Count(a => a.User.id == Temp.account.User.id);
            if (userInCache > 0) { return; }
            Temp.accounts.Add(Temp.account);
            Temp.account.AnswerTests.Clear();
            _scoreRepository.ClearCache();
            _testRepository.ClearCache();
            Temp.tests.Clear();
        }
    }
}
