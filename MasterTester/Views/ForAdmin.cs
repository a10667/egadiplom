﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MasterTester.Entities;
using MasterTester.Repositories;
using Microsoft.Data.SqlClient;

namespace MasterTester
{
    public partial class ForAdmin : Form
    {
        Form1 creator = new Form1();

        private readonly TestRepository _testRepository = new TestRepository();
        private readonly ScoreRepository _scoreRepository = new ScoreRepository();

        public static bool isCountTestChanged = false;

        public ForAdmin()
        {
            InitializeComponent();
        }

        private void ForAdmin_Load(object sender, EventArgs e)
        {
          
            this.Text = Temp.VERSION + " (Права администратора)";
            LabelAccountName.Text = Temp.account.Name;
            ClearAllUILists();
            LoadUsers();
            LoadTests();

        }

        private void ClearAllUILists()
        {
            TestsList.Items.Clear();
            UsersList.Items.Clear();
            UserAnswersList.Items.Clear();
            QuestionsList.Items.Clear();
        }

        public async void LoadTests()
        {
          
            using (var db = new testEntities())
            {
                foreach (var item in db.test)
                {
                    Test t = TestFactory.CreateFrom(item);
                    Temp.tests.Add(t);
                    TestsList.Items.Add(t);
                }
            }

            for (int i1 = 0; i1 < Temp.accounts.Count; i1++)
            {
                Account account = Temp.accounts[i1];
                for (int i = 0; i < Temp.tests.Count; i++)
                {
                    Test test = Temp.tests[i];
                    await _testRepository.FillTestWithUserAnswersAsync(test, account.User.id);
                }
            }
        }

        private void LoadUsers()
        {

            using (var db = new testEntities())
            {
                UsersList.Items.Clear();
                foreach (var u in db.user.Where(u => u.role.name != "admin")) 
                {
                    var cachedUser = (Account)u;
                    UsersList.Items.Add(cachedUser);
                    var userInCache = Temp.accounts.Count(a => a.User.id == cachedUser.User.id);
                    if (userInCache > 0) { continue; }
                    Temp.accounts.Add(cachedUser);
                }
            }
        }

        private void TestsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DeleteTestButton.Enabled = true;
            try
            {
                UserAnswersList.Items.Clear();
                AnswersList.Items.Clear();
                if (TestsList.SelectedItem != null)
                {
                    string selectedTest = TestsList.SelectedItem.ToString();
                    Test selectedTestObject = Temp.tests.FirstOrDefault(test => test.Text == selectedTest);
                    QuestionsList.Items.Clear();
                    foreach (Question question in selectedTestObject.Questions)
                    {
                        QuestionsList.Items.Add(question.Text);
                    }
                    UpdateUserAnswersListAsync();
                    UpdateStats();
                }

                OpenTest.Enabled = true && UsersList.SelectedItem != null;
            }
            catch
            {
            }
        }

        private void UsersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateUserAnswersListAsync();
            UpdateStats();
            DeleteUserButton.Enabled = true;
            OpenTest.Enabled = true && TestsList.SelectedItem != null;
        }

        private void QuestionsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateUserAnswersListAsync();
            AnswersList.Items.Clear();

            int selectedTestIndex = TestsList.SelectedIndex;
            int selectedQuestionIndex = QuestionsList.SelectedIndex;

            if (selectedTestIndex >= 0 && selectedQuestionIndex >= 0)
            {
                Test selectedTest = (Test)TestsList.SelectedItem;
                Question selectedQuestion = selectedTest.Questions[selectedQuestionIndex];


                foreach (Answer answer in selectedQuestion.Answers)
                {
                    string a = "";
                    if (answer.isTrue)
                        a = "☑ " + answer.Text;
                    else
                        a = "☐ " + answer.Text;
                    AnswersList.Items.Add(a);
                }
            }
        }

        private async Task UpdateUserAnswersListAsync()
        {
            int selectedTestIndex = TestsList.SelectedIndex;
            int selectedQuestionIndex = QuestionsList.SelectedIndex;
            int selectedUserIndex = UsersList.SelectedIndex;

            if (selectedTestIndex >= 0 && selectedQuestionIndex >= 0 && selectedUserIndex >= 0)
            {
                Test selectedTest = await _testRepository.FindTestByIdAsync(((Test)TestsList.SelectedItem).IdInt); ;

                Account selectedUser =(Account)UsersList.SelectedItem;

                _ = await _testRepository.FillTestWithUserAnswersAsync(selectedTest, selectedUser.User.id);

                Question selectedQuestion = selectedTest.Questions[selectedQuestionIndex];

                UserAnswersList.Items.Clear();

                var userAnswers = selectedUser.Answers.Where(a => selectedQuestion.Answers.FirstOrDefault(aa => aa.Id == a.Id) != null);
                if (userAnswers.Count() == 0)
                {
                    UserAnswersList.Items.Add(new Answer() { Id = -1, Text = "Не пройден" });
                    return;
                }

                foreach (Answer answer in userAnswers)
                {
                    UserAnswersList.Items.Add(answer);
                }
            }
        }

        private void UpdateStats()
        {
            int selectedTestIndex = TestsList.SelectedIndex;
            if (selectedTestIndex >= 0)
            {
                Test selectedTest = Temp.tests[selectedTestIndex];

                int totalQuestions = selectedTest.Questions.Count;
                int totalAnswers = selectedTest.Questions.SelectMany(q => q.Answers).Count();
                int rightAnswers = 0;

                int selectedUserIndex = UsersList.SelectedIndex;
                if (selectedUserIndex >= 0)
                {
                    Account selectedUser = Temp.accounts[selectedUserIndex];
                    string answerKey = $"TEST{selectedTestIndex}";
                }
            }
        }

        private void TestCreatorButton_Click(object sender, EventArgs e)
        {
            new Creator().ShowDialog();
        }

        private void TestEditorButton_Click(object sender, EventArgs e)
        {
            new Form1().ShowDialog();
        }

        private void DeleteTestButton_Click(object sender, EventArgs e)
        {

        }

        private async void DeleteUserButton_Click(object sender, EventArgs e)
        {
            int selectedIndex = UsersList.SelectedIndex;
            int selectedIndex1 = selectedIndex + 1;
            // Удаление строки с пользователем
            if (selectedIndex >= 0)
                {
                    var selectedUser = UsersList.SelectedItem as Account;

                    using (var db = new testEntities()) 
                    {
                    var user = await db.user.FirstOrDefaultAsync(u => u.id == selectedUser.User.id);
                    if (user == null)
                    { 
                        throw new NullReferenceException(nameof(user));
                    }
                        db.user.Remove(user);
                        db.SaveChanges();
                    }

                    // Удаление пользователя из ListBox
                    UsersList.Items.RemoveAt(selectedIndex);

                    //// Удаление пользователя из списка Temp.accounts
                    //Temp.accounts.RemoveAt(selectedIndex);

                    DeleteUserButton.Enabled = false;

                    MessageBox.Show("Пользователь успешно удален.");
                }
                else
                {
                    MessageBox.Show("Выберите пользователя для удаления.");
                }

            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void ForAdmin_Enter(object sender, EventArgs e)
        {
                TestsList.Items.Clear();
                LoadTests();
        }

        private void ForAdmin_Activated(object sender, EventArgs e)
        {

            TestsList.Items.Clear();
            LoadTests();
        }

        private void OpenTest_Click(object sender, EventArgs e)
        {
            var selectedUser = (Account)UsersList.SelectedItem;
            var selectedTest = (Test)TestsList.SelectedItem;
            
            if (selectedUser == null)
            {
                MessageBox.Show("Выберете пользователя");
                OpenTest.Enabled = false;
                return;
            }

            if (selectedTest == null) 
            {
                MessageBox.Show("Выберете тест");
                OpenTest.Enabled = false;
                return; 
            }

            bool isTestAlreadyOpen = false;
            using (var db = new testEntities()) 
            {
                isTestAlreadyOpen = db.score.FirstOrDefault(s => s.user_id == selectedUser.User.id && s.test_id == selectedTest.IdInt) == null;
            }
                
            if (isTestAlreadyOpen)
            {
                MessageBox.Show("Тест уже открыт");
                return;
            }

            using (var db = new testEntities())
            {

                db.Database.ExecuteSqlCommand($"delete from [score] where test_id = {selectedTest.Id} and user_id = {selectedUser.User.id}");
                
                foreach (var question in selectedTest.Questions)
                {
                    foreach (var answer in question.Answers)
                    {
                        db.Database.ExecuteSqlCommand($"delete from [user_answer] where answer_id = {answer.Id} and user_id = {selectedUser.User.id}");
                        var userAnswer = selectedUser.Answers.FirstOrDefault(a => a.Id == answer.Id);
                        selectedUser.Answers.Remove(userAnswer);
                    }
                
                }
                db.SaveChanges();
            }
            UserAnswersList.Items.Clear ();

            MessageBox.Show("Тест открыт!");
        }

        private void ForAdmin_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClearAllUILists();
            Temp.ClearAll();
        }

        private void DeleteTestButton_Click_1(object sender, EventArgs e)
        {
            if (TestsList.SelectedItem == null) return;

            Test selectedTest = (Test)TestsList.SelectedItem;
            TestsList.Items.Remove(selectedTest);
            QuestionsList.Items.Clear();
            AnswersList.Items.Clear();
            UserAnswersList.Items.Clear();
            _testRepository.DeleteTestWithId(selectedTest.IdInt);
            DeleteTestButton.Enabled = false;

        }
    }
}
