﻿namespace MasterTester
{
    partial class Creator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Creator));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Code = new System.Windows.Forms.TextBox();
            this.AddOneTrue = new System.Windows.Forms.Button();
            this.AddSomeTrue = new System.Windows.Forms.Button();
            this.AddFreeAnswer = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(37)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(668, 356);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 204);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Code
            // 
            this.Code.AcceptsTab = true;
            this.Code.Location = new System.Drawing.Point(13, 12);
            this.Code.Multiline = true;
            this.Code.Name = "Code";
            this.Code.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Code.Size = new System.Drawing.Size(650, 548);
            this.Code.TabIndex = 1;
            this.Code.TextChanged += new System.EventHandler(this.Code_TextChanged);
            // 
            // AddOneTrue
            // 
            this.AddOneTrue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.AddOneTrue.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AddOneTrue.Font = new System.Drawing.Font("Bahnschrift Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddOneTrue.ForeColor = System.Drawing.Color.White;
            this.AddOneTrue.Location = new System.Drawing.Point(6, 19);
            this.AddOneTrue.Name = "AddOneTrue";
            this.AddOneTrue.Size = new System.Drawing.Size(188, 23);
            this.AddOneTrue.TabIndex = 2;
            this.AddOneTrue.Text = "Один верный ответ";
            this.AddOneTrue.UseVisualStyleBackColor = false;
            this.AddOneTrue.Click += new System.EventHandler(this.AddOneTrue_Click);
            // 
            // AddSomeTrue
            // 
            this.AddSomeTrue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.AddSomeTrue.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AddSomeTrue.Font = new System.Drawing.Font("Bahnschrift Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddSomeTrue.ForeColor = System.Drawing.Color.White;
            this.AddSomeTrue.Location = new System.Drawing.Point(6, 48);
            this.AddSomeTrue.Name = "AddSomeTrue";
            this.AddSomeTrue.Size = new System.Drawing.Size(188, 23);
            this.AddSomeTrue.TabIndex = 3;
            this.AddSomeTrue.Text = "Несколько верных ответов";
            this.AddSomeTrue.UseVisualStyleBackColor = false;
            this.AddSomeTrue.Click += new System.EventHandler(this.AddSomeTrue_Click);
            // 
            // AddFreeAnswer
            // 
            this.AddFreeAnswer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.AddFreeAnswer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AddFreeAnswer.Font = new System.Drawing.Font("Bahnschrift Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddFreeAnswer.ForeColor = System.Drawing.Color.White;
            this.AddFreeAnswer.Location = new System.Drawing.Point(6, 77);
            this.AddFreeAnswer.Name = "AddFreeAnswer";
            this.AddFreeAnswer.Size = new System.Drawing.Size(188, 23);
            this.AddFreeAnswer.TabIndex = 4;
            this.AddFreeAnswer.Text = "Свободный ответ";
            this.AddFreeAnswer.UseVisualStyleBackColor = false;
            this.AddFreeAnswer.Click += new System.EventHandler(this.AddFreeAnswer_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.SaveButton.Enabled = false;
            this.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveButton.Font = new System.Drawing.Font("Bahnschrift Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SaveButton.ForeColor = System.Drawing.Color.White;
            this.SaveButton.Location = new System.Drawing.Point(668, 125);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(94, 23);
            this.SaveButton.TabIndex = 5;
            this.SaveButton.Text = "Сохранить";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(71)))), ((int)(((byte)(81)))));
            this.DeleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DeleteButton.Font = new System.Drawing.Font("Bahnschrift Condensed", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteButton.ForeColor = System.Drawing.Color.White;
            this.DeleteButton.Location = new System.Drawing.Point(774, 125);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(94, 23);
            this.DeleteButton.TabIndex = 6;
            this.DeleteButton.Text = "Отчистить";
            this.DeleteButton.UseVisualStyleBackColor = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AddOneTrue);
            this.groupBox1.Controls.Add(this.AddSomeTrue);
            this.groupBox1.Controls.Add(this.AddFreeAnswer);
            this.groupBox1.Font = new System.Drawing.Font("Bahnschrift Condensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(668, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 107);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Добавить вопрос";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(37)))));
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(669, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 198);
            this.label1.TabIndex = 8;
            this.label1.Text = "Справка:\r\n\r\nПожалуйста соблюдайте разметку.\r\n\r\nПишите имена для теста, вопросов и" +
    " ответов вместо текста поясняющего функционал параметров.\r\n";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Creator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(17)))), ((int)(((byte)(23)))));
            this.ClientSize = new System.Drawing.Size(873, 572);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.Code);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Creator";
            this.Text = "Мастер Тестер (Редактор тестов)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Creator_FormClosing);
            this.Load += new System.EventHandler(this.Creator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox Code;
        private System.Windows.Forms.Button AddOneTrue;
        private System.Windows.Forms.Button AddSomeTrue;
        private System.Windows.Forms.Button AddFreeAnswer;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
    }
}