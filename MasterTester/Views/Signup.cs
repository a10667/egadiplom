﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MasterTester.Entities;
using Microsoft.Data.SqlClient;
using MasterTester.Models;
using Group = MasterTester.Models.Group;

namespace MasterTester
{
    public partial class Signup : Form
    {
        public List<string> lineSQL;

        public Signup()
        {
            InitializeComponent();
        }

        private void Signup_Load(object sender, EventArgs e)
        {
           
            ButtonReg.Enabled = false;
            
            using (var db = new testEntities())
            {

                var groupsInMem = new List<Group>();
                foreach (var userGroup in db.user_group)
                {
                    var g = new Group()
                    {
                        Id = userGroup.group_id,
                        Year = userGroup.year,
                        Name = userGroup.group.abbriviation,
                        Accounts = new List<Account>()

                    };
                    var groupInTmp = groupsInMem.FirstOrDefault(gm => gm.Id == g.Id && gm.Year == g.Year);
                    if (groupInTmp is null)
                    {
                        g.Accounts.Add((Account)userGroup.user);
                        groupsInMem.Add(g);
                    }
                    else
                    {
                        groupInTmp.Accounts.Add((Account)userGroup.user);
                    }

                }

                foreach (var g in groupsInMem)
                {
                    GroupBox.Items.Add(g);
                }
            }
        }

        private void ButtonReg_Click(object sender, EventArgs e)
        {
            // Получить значения из текстовых полей
            string name = TextBoxName.Text.Trim();
            string sname = SecondNameTextBox.Text.Trim();
            string lname = LastNameTextBox.Text.Trim();
            string login = TextBoxLogin.Text.Trim();
            string password = TextBoxPassword.Text.Trim();

            using (var db = new testEntities())
            {
                var user = db.user.FirstOrDefault(u => u.login == login);
                if (user != null)
                {
                    MessageBox.Show("Пользователь уже существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            // Проверить, что пароли совпадают
            if (password != TextBoxConfirmPassword.Text.Trim())
            {
                IndicatorLabel.ForeColor = Color.DarkRed;
                IndicatorLabel.Text = "Пароли не совпадают.";
                return;
            }

            // Проверить, что пароль соответствует требованиям
            if (!ValidatePassword(password))
            {
                IndicatorLabel.ForeColor = Color.DarkRed;
                IndicatorLabel.Text = "Пароль не соответствует требованиям.";
                return;
            }

            var selectedGroup = GroupBox.SelectedItem as Group;
            if (selectedGroup is null)
            {
                MessageBox.Show($"Выберете группу.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

            using (var db = new testEntities())
            {


                var user = new user() { login = login, password = password, role_id = (int?)Account.Role.User};
                db.user.Add(user);
                db.SaveChanges();
                var userInfo = new user_info() 
                {
                    user_id = db.user.OrderByDescending(u => u.id).First().id,
                    firstname = name,
                    secondname = sname,
                    lastname = lname,
                };
                db.user_info.Add(userInfo);
                db.SaveChanges();
 

                db.user_group.Add(new user_group { 
                    group_id = selectedGroup.Id,
                    user_id = db.user.OrderByDescending(u => u.id).First().id,
                    year = selectedGroup.Year,
                });
                db.SaveChanges();
            }


                // Очистить текстовые поля
                TextBoxName.Clear();
            TextBoxLogin.Clear();
            TextBoxPassword.Clear();
            TextBoxConfirmPassword.Clear();
            SecondNameTextBox.Clear();
            LastNameTextBox.Clear();

            IndicatorLabel.ForeColor = Color.DarkGreen;
            IndicatorLabel.Text = "Регистрация успешна!";
        }

        private bool ValidatePassword(string password)
        {
            // Проверить длину пароля
            if (password.Length < 8)
            {
                MessageBox.Show($"Пароль короче 8 символов.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            // Проверить, содержит ли пароль хотя бы одну букву и цифру
            if (!password.Any(char.IsLetter) || !password.Any(char.IsDigit))
            {
                MessageBox.Show($"В пароле отсутствуют либо цифры, либо буквы.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            var specialChars = "!@#$%^&*";
            if (password.Where(c => specialChars.Contains(c)).Any()) 
            {
                MessageBox.Show($"В пароле есть специальные символы.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void CheckIfAllFieldsFilled()
        {
            // Проверить, заполнены ли все текстовые поля
            bool allFieldsFilled = TextBoxName.Text.Trim().Length > 0 &&
                                   SecondNameTextBox.Text.Trim().Length > 0 &&
                                   LastNameTextBox.Text.Trim().Length > 0 &&
                                   TextBoxLogin.Text.Trim().Length > 0 &&
                                   TextBoxPassword.Text.Trim().Length > 0 &&
                                   TextBoxConfirmPassword.Text.Trim().Length > 0;

            // Разблокировать кнопку регистрации, если все поля заполнены
            ButtonReg.Enabled = allFieldsFilled;
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            CheckIfAllFieldsFilled();
        }

        private void TextBoxConfirmPassword_TextChanged(object sender, EventArgs e)
        {
            CheckIfAllFieldsFilled();
        }

        private void TextBoxPassword_TextChanged(object sender, EventArgs e)
        {
            CheckIfAllFieldsFilled();
        }

        private void TextBoxLogin_TextChanged(object sender, EventArgs e)
        {
            CheckIfAllFieldsFilled();
        }

        private void TextBoxName_TextChanged(object sender, EventArgs e)
        {
            CheckIfAllFieldsFilled();
        }

        private void SecondNameTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckIfAllFieldsFilled();
        }

        private void LastNameTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckIfAllFieldsFilled();
        }
    }
}
