﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterTester.Models
{
    internal class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        
        public List<Account> Accounts { get; set; }

        public override string ToString()
        {
            if (Year < 1) return $"{Name}";
            return $"{Year} {Name}";
        }
    }
}
