﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterTester.Models
{
    internal class Score
    {
        public Score(int id, Account account, Test test)
        {
            Id = id;
            Account = account;
            Test = test;
        }

        public int Id { get; }
        public Account Account { get; }
        public Test Test { get; }

    }
}
