﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace MasterTester
{
    internal static class Temp
    {
        public static string VERSION = "Мастер Тестер v0.9.3";

        public static Account account = new Account();
        public static List<Account> accounts = new List<Account>();
        public static List<Test> tests = new List<Test>();

        public static List<string> currentTets = new List<string>();
        public static int currentTestIndex = -1;


        public static List<Test> ParseTest(string[] lines)
        {
            List<Test> tests = new List<Test>();
            
            Test currentTest = null;
            Question currentQuestion = null;
    

            foreach (string line in lines)
            {
                switch (line)
                {
                    case var s when s.Contains("TEST = "):
                        currentTest = new Test();
                        currentTest.Text = line.Substring("TEST = ".Length);
                        tests.Add(currentTest);
                        break;
                    case var s when s.Contains("ID = "):
                        if (currentTest == null) 
                        {
                            throw new Exception("Вы забыли название теста или стерли пробел после '='");
                        }
                        currentTest.Id = line.Substring("ID = ".Length + 1);
                        break;
                    case var s when s.Contains("TIME = "):
                        currentTest.Time = int.Parse(line.Substring("TIME = ".Length));
                        break;
                    case var s when s.Contains("QUESTION_ONE_TRUE = "):
                        currentQuestion = new Question();
                        currentQuestion.Type = QuestionType.ONE_TRUE;
                        currentQuestion.Text = line.Substring("QUESTION_ONE_TRUE = ".Length);
                        currentTest.Questions.Add(currentQuestion);
                        break;
                    case var s when s.Contains("QUESTION_FREE_ANSWER = "):
                        currentQuestion = new Question();
                        currentQuestion.Type = QuestionType.FREE_ANSWER;
                        currentQuestion.Text = line.Substring("QUESTION_FREE_ANSWER = ".Length);
                        currentTest.Questions.Add(currentQuestion);
                        break;
                    case var s when s.Contains("QUESTION_SOME_TRUE = "):
                        currentQuestion = new Question();
                        currentQuestion.Type = QuestionType.SOME_TRUE;
                        currentQuestion.Text = line.Substring("QUESTION_SOME_TRUE = ".Length);
                        currentTest.Questions.Add(currentQuestion);
                        break;
                    case var s when (s.Contains("ANSWER_TRUE = ") || s.Contains("ANSWER_FALSE = ")) && currentQuestion != null:
                        Answer answer = new Answer();
                        answer.Text = line.Substring(line.IndexOf('=') + 2); 
                        answer.isTrue = line.Contains("ANSWER_TRUE");
                        answer.isAlwaysTrue = line.Contains("ALWAYS_TRUE");
                        currentQuestion.Answers.Add(answer);
                        break;
                    case "}":
                        currentQuestion = null;
                        break;
                }
            }

            return tests;
        }


        public static void ClearAll()
        {
            accounts.Clear();
            tests.Clear();
        }
    }
}
