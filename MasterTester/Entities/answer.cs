//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MasterTester.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class answer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public answer()
        {
            this.user_answer = new HashSet<user_answer>();
        }
    
        public int id { get; set; }
        public string value { get; set; }
        public Nullable<int> question_id { get; set; }
        public Nullable<bool> is_right { get; set; }
    
        public virtual question question { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<user_answer> user_answer { get; set; }
    }
}
