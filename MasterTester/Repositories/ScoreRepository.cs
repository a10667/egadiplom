﻿using MasterTester.Entities;
using MasterTester.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterTester.Repositories
{
    internal class ScoreRepository
    {
        private static readonly Lazy<Dictionary<int, Score>> _cachedScores = new Lazy<Dictionary<int, Score>>(() => new Dictionary<int, Score>()); 

        public async Task<Score> FindTestScoreOfUserByIdAsync(int userId, int testId) 
        {

            var scoreInCache = FindScoreInCache(s => s.Test.IdInt == testId && s.Account.User.id == userId);
            if (scoreInCache != null)
                return scoreInCache;

            using (var db = new testEntities()) 
            {
                var score = (await db.score.FirstOrDefaultAsync(s => s.test_id == testId && s.user_id == userId));
                if (score == null)
                {
                    return null;
                }

                _cachedScores.Value.Add(score.id, new Score(score.id, score.user, TestFactory.CreateFrom(score.test)));

                return _cachedScores.Value[score.id];
            }
        }

        private Score FindScoreInCache(Func<Score, bool> predicate) 
        {
            if (_cachedScores.Value.Count <= 0) 
                return null;

            var scoreInCache = _cachedScores.Value.Values.FirstOrDefault(predicate);
            if (scoreInCache != null)
                return _cachedScores.Value[scoreInCache.Id];
            return null;
        }


        public void ClearCache()
        {
            _cachedScores.Value.Clear();
        }
    }
}
