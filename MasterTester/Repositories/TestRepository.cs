﻿using MasterTester.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace MasterTester.Repositories
{
    internal class TestRepository
    {
        private static Lazy<Dictionary<int, Test>> _cachedTests = new Lazy<Dictionary<int, Test>>(() => new Dictionary<int, Test>());

        public async Task<Test> FindTestByIdAsync(int id)
        {
            var cachedTest = _cachedTests.Value.Values.FirstOrDefault(t => t.IdInt == id);
            if (cachedTest != null)
            {
                return cachedTest;
            }

            using (var db = new testEntities())
            {
                var testInDb = await db.test.FirstOrDefaultAsync(t => t.id == id);
                if (testInDb == null)
                {
                    return null;
                }
                var testOutDb = TestFactory.CreateFrom(testInDb);
                _cachedTests.Value.Add(testInDb.id, testOutDb);
                return testOutDb;
            }
        }

        public async Task<Test> FillTestWithUserAnswersAsync(Test test, int userId)
        {
            if (!_cachedTests.Value.ContainsKey(test.IdInt)) 
            {
                _cachedTests.Value.Add(test.IdInt, test);
            }

            if(test.Questions.SelectMany(q => q.Answers).Any(a => a.isUserAnswer)) { return test; }

            using (var db = new testEntities())
            {
                var user = await db.user.FirstOrDefaultAsync(u => u.id == userId);
                if (user == null) 
                {
                    return test;
                }
                foreach (var question in test.Questions)
                {
                    var userAnswers = user.user_answer.Where(a => a.answer.question_id == question.Id);
                    foreach (var userAnswer in userAnswers)
                    {
                        question.Answers.FirstOrDefault(a => a.Id== userAnswer.answer.id).isUserAnswer = true;
                    }
                }
                return test;
            }
        }

        public void ClearCache()
        { 
            _cachedTests.Value.Clear();
        }

        internal void DeleteTestWithId(int idInt)
        {
            using (var db = new testEntities()) 
            {
                var test = db.test.First(a => a.id == idInt);
                db.test.Remove(test);
                db.SaveChanges();
            }
        }
    }
}
