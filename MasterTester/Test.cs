﻿using MasterTester.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace MasterTester
{
    class Test
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public int Time { get; set; }

        public int IdInt => int.Parse(Id);
        public List<Question> Questions { get; set; }

        public Test()
        {
            Questions = new List<Question>();
        }

        public override string ToString()
        {
            return Text;
        }
    }

    class Question
    {
        public string Text { get; set; }
        public QuestionType Type { get; set; }

        public List<Answer> Answers { get; set; }
        public int Id { get; internal set; }

        public Question()
        {
            Answers = new List<Answer>();
        }
    }

    class Answer
    {
        public string Text { get; set; }
        public bool isTrue { get; set; }
        public bool isAlwaysTrue { get; set; }
        public bool isUserAnswer { get; set; }
        public int Id { get; internal set; }

        public Answer()
        {
            this.isUserAnswer = false;
        }

        public override string ToString()
        {
            return (isTrue? "☑ ": "☒ ") + Text;
        }

    }

    public enum QuestionType
    {
        ONE_TRUE = 3,
        FREE_ANSWER = 2,
        SOME_TRUE = 4,
        NO_TYPE
    }


    internal class TestBuilder
    {
        private Test _test = new Test();

        public TestBuilder WithId(int id)
        {
            _test.Id = id.ToString();
            return this;
        }

        public TestBuilder WithText(string id)
        {
            _test.Text = id.ToString();
            return this;
        }

        public TestBuilder WithTime(int id)
        {
            _test.Time = id;
            return this;
        }

        public TestBuilder AddQuestion(Func<QuestionBuilder, Question> builder)
        {
            _test.Questions.Add(builder(new QuestionBuilder()));
            return this;
        }

        public static implicit operator Test(TestBuilder ts)
        {
            return ts._test;
        }
    }

    internal class QuestionBuilder : TestBuilder
    {
        private Question _question = new Question();

        public new QuestionBuilder WithText(string id)
        {
            _question.Text = id.ToString();
            return this;
        }
        public new QuestionBuilder WithId(int id)
        {
            _question.Id = id;
            return this;
        }
        public QuestionBuilder AddAnswer(Func<AnswerBuilder, Answer> builder)
        {
            _question.Answers.Add(builder(new AnswerBuilder()));
            return this;
        }

        public QuestionBuilder WithType(string id)
        {

            if (id == "free") { _question.Type = QuestionType.FREE_ANSWER; }
            else if (id == "one") { _question.Type = QuestionType.ONE_TRUE; }
            else if (id == "many") { _question.Type = QuestionType.SOME_TRUE; }
            return this;
        }

        internal Question Build()
        {
            return  _question;
        }


        public static implicit operator Question(QuestionBuilder builder) { return builder._question; }
    }

    internal class AnswerBuilder : TestBuilder
    {
        private Answer _answer = new Answer();

        public AnswerBuilder WithId(int id)
        {
            _answer.Id = id;
            return this;
        }

        public AnswerBuilder WithText(string id)
        {
            _answer.Text = id.ToString();
            return this;
        }

        public AnswerBuilder IsTrue(bool id)
        {
            _answer.isTrue = id;
            return this;
        }

        public static implicit operator Answer(AnswerBuilder builder) { return builder._answer; }
    }

    internal static class TestFactory 
    {
        public static Test CreateFrom(test test) 
        {
            var tb = new TestBuilder();
            tb.WithText(test.name).WithId(test.id);
            foreach (var q in test.question)
            {
                tb.AddQuestion(qq => {
                    foreach (var a in q.answer)
                    {
                        qq.AddAnswer(aa => aa.IsTrue(a.is_right.Value).WithText(a.value).WithId(a.id));
                    }
                    return qq.WithText(q.name).WithId(q.id).WithType(q.question_type.name);
                });
            }
            return tb;
        }
    }
}
