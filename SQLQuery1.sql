create database test;
go 

use test; 
go 

create table [role] (
	[id] int identity not null primary key,
	[name] nvarchar(max) not null,
); 
go  

create table [user] (
	[id] int identity not null primary key,
	[role_id] int foreign key REFERENCES [role]([id]) on delete cascade on update cascade,
	[login] nvarchar(50) not null, 
	[password] nvarchar(50) not null,
);
go

create table [user_info] (
	[id] int identity not null primary key,
	[firstname] nvarchar(50) not null,
	[secondname] nvarchar(50) not null,
	[lastname] nvarchar(50) not null,
	[user_id] int foreign key REFERENCES [user]([id]) on delete cascade on update cascade,
); 
go

create table [test] (
	[id] int identity not null primary key,
	[name] nvarchar(max) not null,
	[is_open] bit default(1)
);
go

create table [question_type] (
	[id] int identity not null primary key,
	[name]nvarchar(50) not null,
); 
go  

create table [question] (
	[id] int identity not null primary key,
	[name] nvarchar(max) not null,
	[test_id] int foreign key REFERENCES [test]([id]) on delete cascade on update cascade,
	[question_type_id] int foreign key REFERENCES [question_type]([id]) on delete cascade on update cascade
); 
go


create table [answer] (
	[id] int identity not null primary key,
	[value] nvarchar(max) not null,
	[question_id] int foreign key  REFERENCES [question]([id]) on delete cascade on update cascade,
	[is_right] bit default(0)
); 
go

create table [score] (
	[id] int identity not null primary key,
	[value] decimal not null,
	[test_id] int foreign key REFERENCES [test]([id]) on delete cascade on update cascade,
	[user_id] int foreign key REFERENCES [user]([id]) on delete cascade on update cascade,
); 
go

create table [user_answer] (
	[user_id] int foreign key REFERENCES [user]([id]) on delete cascade on update cascade,
	[answer_id] int foreign key REFERENCES [answer]([id]) on delete cascade on update cascade,
	primary key([user_id], [answer_id])
);
go